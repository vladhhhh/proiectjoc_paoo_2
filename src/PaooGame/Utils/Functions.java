package PaooGame.Utils;

import PaooGame.Graphics.Animation;

import java.awt.image.BufferedImage;

/*! \class Functions
    \brief Aceasta clasa contine diverse functii folositoare , in mod special pentru prelucrare de imagini si animatii.
    */
public class Functions {

    /*! \fn public static BufferedImage mirrorImage(BufferedImage img){
    \brief Functie pentru a intoarce o imagine fata de axa verticala.

        Functia primeste o imagine ce urmeaza a fi procesata pentru a fi "in oglinda", adica
        simetrica fata de axa verticala. Aceasta ia pixelii din marginea exterioara din dreapta
        si, intr-o imagine noua, ii pune in marginea din stanga.
    */
    public static BufferedImage mirrorImage(BufferedImage img){
        int width = img.getWidth();
        int height = img.getHeight();
        BufferedImage mimg = new BufferedImage(width*2, height, img.getType());
        for(int y = 0; y < height; y++){
            for(int lx = 0, rx = width*2 - 1; lx < width; lx++, rx--){
                int p = img.getRGB(lx, y);
                mimg.setRGB(lx, y, p);
                mimg.setRGB(rx, y, p);
            }
        }
        return mimg.getSubimage(width,0,width,height);
    }

    /*! \fn public static Animation reverse_spritesheet_img(Animation ani){
    \brief Functie ce oglindeste o animatie imagine cu imagine.

        Functia primeste o animatie "ani", iar imaginile individuale vor fi oglindite,
        astfel, animatia pastrandu-si ordinea corecta de afisare dar cu imaginile din animatie
        oglindite.
    */
    public static Animation reverse_spritesheet_img(Animation ani){
        BufferedImage img;
        int n = ani.getSize();
        for (int i = 0; i < n; i++) {
            img = ani.getFrame();
            ani.setFrame(i,mirrorImage(img));
            ani.nextFrame();
        }
        return ani;
    }
}
