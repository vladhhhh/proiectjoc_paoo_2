package PaooGame.Utils;

/*! \class Direction
    \brief Clasa contine directiile pe care le poate avea un caracter
 */

public enum Direction {
    LEFT,RIGHT,UP,DOWN
}
