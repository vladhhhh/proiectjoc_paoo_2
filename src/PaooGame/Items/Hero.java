package PaooGame.Items;

import java.awt.*;
import java.sql.*;

import PaooGame.Graphics.Animation;
import PaooGame.RefLinks;
import PaooGame.Graphics.Assets;
import PaooGame.States.PlayState;
import PaooGame.States.State;
import PaooGame.Utils.Direction;

/*! \class public class Hero extends Character
    \brief Implementeaza notiunea de erou/player (caracterul controlat de jucator).

    Elementele suplimentare pe care le aduce fata de clasa de baza sunt:
        imaginea (acest atribut poate fi ridicat si in clasa de baza)
        deplasarea
        atacul (nu este implementat momentan)
        dreptunghiul de coliziune
 */
public class Hero extends Character
{
    private Animation sabie;    /*!< Referinta catre animatia curenta a armei eroului.*/
    protected int gold;

    /*! \fn public Hero(RefLinks refLink, float x, float y)
        \brief Constructorul de initializare al clasei Hero.

        \param refLink Referinta catre obiectul shortcut (obiect ce retine o serie de referinte din program).
        \param x Pozitia initiala pe axa X a eroului.
        \param y Pozitia initiala pe axa Y a eroului.
     */
    public Hero(RefLinks refLink, float x, float y)
    {
            ///Apel al constructorului clasei de baza
        super(refLink, x,y, Character.DEFAULT_CREATURE_WIDTH, Character.DEFAULT_CREATURE_HEIGHT);
        gold = 0;
            ///Seteaza animatia de start a eroului
        anim_idle = Assets.heroRight_idle;
        anim_running = Assets.heroRight;
        anim = Assets.heroRight_idle;
        sabie = Assets.sword_anim_right;
            ///Seteaza directia de start a eroului
        dir = Direction.RIGHT;
            ///Stabilieste pozitia relativa si dimensiunea dreptunghiului de coliziune, starea implicita(normala)
        normalBounds.x = 12;
        normalBounds.y = 12;
        normalBounds.width = 24;
        normalBounds.height = 24;

            ///Stabilieste pozitia relativa si dimensiunea dreptunghiului de coliziune, starea de atac
        attackBounds.x = 10;
        attackBounds.y = 10;
        attackBounds.width = 38;
        attackBounds.height = 38;
    }

    /*! \fn public void Update()
        \brief Actualizeaza pozitia si imaginea eroului.
     */
    @Override
    public void Update()
    {
            ///Verifica daca a fost apasata o tasta
        GetInput();
            ///Actualizeaza animatia
        AnimUpdate();
            ///Actualizeaza pozitia
        Move();

    }

    /*! \fn private void AnimUpdate()
        \brief Actualizeaza animatia actuala a eroului.
     */
    @Override
    protected void AnimUpdate(){

        ///Actualizeaza animatia
        switch (dir){
            case LEFT: anim_idle = Assets.heroLeft_idle; anim_running=Assets.heroLeft; sabie = Assets.sword_anim_left; break;
            case RIGHT: anim_idle = Assets.heroRight_idle; anim_running=Assets.heroRight;sabie = Assets.sword_anim_right; break;
            case UP : sabie = Assets.sword_anim_up; break;
            case DOWN: sabie = Assets.sword_anim_down; break;
        }
        if(hasAttacked > 0){
            hasAttacked--;
        }
        if(isIdle > 0 && !isMoving()) {
            isIdle--;
        }
        else if(isIdle<=0) {
            anim = anim_idle;
            isIdle--;
            if(isIdle <= -5){
                anim.nextFrame();
                isIdle=0;
            }
        }

        if(isMoving()){
            isIdle = 100;
            if(run <= 10 && run >= 0){
                run--;
            }
            else if (run <0){
                anim.nextFrame();
                run = 10;
            }
            anim = anim_running;
        }
        else{
            anim = anim_idle;
            run = 10;
        }
    }


    /*! \fn private void GetInput()
            \brief Verifica daca a fost apasata o tasta din cele stabilite pentru controlul eroului.
     */
    private void GetInput()
    {
            ///Implicit eroul nu trebuie sa se deplaseze daca nu este apasata o tasta
        xMove = 0;
        yMove = 0;
            ///Verificare apasare tasta "sus"
        if(refLink.GetKeyManager().up  && canMoveUp())
        {
            yMove = -speed;
            dir = Direction.UP;
        }
            ///Verificare apasare tasta "jos"
        if(refLink.GetKeyManager().down && canMoveDown())
        {
            yMove = speed;
            dir = Direction.DOWN;
        }
            ///Verificare apasare tasta "left"
        if(refLink.GetKeyManager().left && canMoveLeft())
        {
            xMove = -speed;
            dir = Direction.LEFT;
        }
            ///Verificare apasare tasta "dreapta"
        if(refLink.GetKeyManager().right && canMoveRight())
        {
            xMove = speed;
            dir = Direction.RIGHT;
        }
        ///Verificare apasare tasta "atac"
        if(refLink.GetKeyManager().attack)
        {
            attack();
        }

        if(refLink.GetKeyManager().exitGame){
            SaveGame();
            System.exit(0);
        }

        if(refLink.GetKeyManager().SaveGame){
           SaveGame();
        }
    }

    /*! \fn private boolean isMoving()
        \brief Verifica daca eroul se misca.
    */
    private boolean isMoving(){
        return (refLink.GetKeyManager().right || refLink.GetKeyManager().left || refLink.GetKeyManager().up || refLink.GetKeyManager().down || refLink.GetKeyManager().attack);
    }


    /*! \fn public void Draw(Graphics g)
        \brief Randeaza/deseneaza eroul in noua pozitie.

        \brief g Contextul grafic in care trebuie efectuata desenarea eroului.
     */
    @Override
    public void Draw(Graphics g)
    {
        drawAttack(g);
        g.drawImage(anim.getFrame(), refLink.GetWidth()/2 - anim.getFrame().getWidth()/2, refLink.GetHeight()/2 - anim.getFrame().getHeight()/2, width, height, null);
    }

    /*! \fn private void drawAttack(Graphics g){
    \brief Deseneaza eroul atunci cand acesta ataca.

    \brief g Contextul grafic in care trebuie efectuata desenarea eroului.
    */
    private void drawAttack(Graphics g){
        if(hasAttacked > 0 ){
            if(hasAttacked % (30/sabie.getSize()) == 0)
                sabie.nextFrame();
            int pos_x, pos_y;
            switch (dir){
                case UP: pos_x = 0; pos_y = -24; break;
                case LEFT: pos_x = -36; pos_y = -6; break;
                case DOWN: pos_x = 0; pos_y = 24; break;
                case RIGHT: pos_x = 36; pos_y = -6; break;
                default: pos_x = 0; pos_y = 0; break;
            }
            g.drawImage(sabie.getFrame(),refLink.GetWidth()/2 - anim.getFrame().getWidth()/2+pos_x, refLink.GetHeight()/2 - anim.getFrame().getHeight()/2+pos_y, width, height, null);
        }
    }

    /*! \fn public void attack()
    \brief Initiaza atacul eroului.
    */
    @Override
    public void attack() {
        if(hasAttacked < 0 ){
            hasAttacked = 30;
            sabie.resetAni();
        }
        hasAttacked--;
    }

    /*! \fn public int getGold()
    \brief Returneaza numarul de bani al eroului.
    */
    public int getGold() {
        return gold;
    }

    /*! \fn public int setGold(int gold)
    \brief Seteaza numarul de bani al eroului.

    \brief gold Numarul de bani pe care eroul il va avea
    */
    public void setGold(int gold) {
        this.gold = gold;
    }

    /*! \fn  public void SaveGame()
    \brief Returneaza numarul de bani al eroului.
    */
    public void SaveGame(){
        /// Salveaza harta in fisier.
        refLink.GetMap().SaveGame_Map();
        Statement st = null;
        /// Se creeaza conexiunea catre DB
        Connection c = refLink.getConnection();
        try {
            /// Salveaza datele despre inamici in DB
            PlayState.SaveGame_Enemies();
            /// Salvam datele jucatorului in DB
            st = c.createStatement();
            String sql = "UPDATE PLAYER_INFO set X = " + x + " WHERE ID = 1;";
            System.out.println("UPDATE X " + st.executeUpdate(sql));
            sql = "UPDATE PLAYER_INFO set Y = " + y + " WHERE ID = 1;";
            System.out.println("UPDATE Y " + st.executeUpdate(sql));
            sql = "UPDATE PLAYER_INFO set HP = " + life + " WHERE ID = 1;";
            System.out.println("UPDATE HP " + st.executeUpdate(sql));
            sql = "UPDATE PLAYER_INFO set GOLD = " + gold + " WHERE ID = 1;";
            System.out.println("UPDATE GOLD " + st.executeUpdate(sql));
            st.close();
            c.commit();
        }
        catch (Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
}
