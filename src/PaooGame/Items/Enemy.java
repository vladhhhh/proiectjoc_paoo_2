package PaooGame.Items;

import PaooGame.Graphics.Assets;
import PaooGame.RefLinks;
import PaooGame.States.PlayState;
import PaooGame.Utils.Direction;

import java.awt.*;
import java.util.Random;

import static java.lang.Math.abs;

public class Enemy extends Character{

    public String type;

    /*! \fn public Enemy(RefLinks refLink, float x, float y)
        \brief Constructorul de initializare al clasei Enemy.

        \param refLink Referinta catre obiectul shortcut (obiect ce retine o serie de referinte din program).
        \param x Pozitia initiala pe axa X a eroului.
        \param y Pozitia initiala pe axa Y a eroului.
     */
    public Enemy(RefLinks refLink, float x, float y, String type_given)
    {
        ///Apel al constructorului clasei de baza
        super(refLink, x,y, Character.DEFAULT_CREATURE_WIDTH, Character.DEFAULT_CREATURE_HEIGHT);
        type = type_given;
        ///Seteaza animatia de start a eroului
        switch (type){
            case "snake":
                anim_idle = Assets.snakeRight_idle;
                anim_running = Assets.snakeRight;
                anim_attack = Assets.snakeRight_attack;
                anim = Assets.snakeRight_idle;
                break;

            case "scorpio":
                anim_idle = Assets.scorpioRight_idle;
                anim_running = Assets.scorpioRight;
                anim_attack = Assets.scorpioRight_attack;
                anim = Assets.scorpioRight_idle;
                break;

            case "vulture":
                anim_idle = Assets.vultureRight_idle;
                anim_running = Assets.vultureRight;
                anim_attack = Assets.vultureRight_attack;
                anim = Assets.vultureRight_idle;
                break;

        }
        ///Seteaza directia de start a inamicului
        dir = Direction.RIGHT;
        speed = 1.0f;
        ///Stabilieste pozitia relativa si dimensiunea dreptunghiului de coliziune, starea implicita(normala)
        normalBounds.x = 12;
        normalBounds.y = 12;
        normalBounds.width = 24;
        normalBounds.height = 24;

        ///Stabilieste pozitia relativa si dimensiunea dreptunghiului de coliziune, starea de atac
        attackBounds.x = 10;
        attackBounds.y = 10;
        attackBounds.width = 27;
        attackBounds.height = 27;
    }


    /*! \fn public void attack()
        \brief Metoda ce defineste atacul inamicului in momentul in care acesta ataca;
    */
    @Override
    public void attack() {
        ///Verifica daca jucatorul este in zona in primul si in primul rand, daca acesta nu s-a mutat de atunci pana acum
        if(isPlayerNearby()){
                anim = anim_attack;
                if(hasAttacked <= anim_attack.getSize()*anim_attack.getSize()+anim_attack.getSize()-1){
                    PlayState.hero.life--;
                    anim.resetAni();
                    hasAttacked = anim_attack.getSize()*anim_attack.getSize()+anim_attack.getSize()-1;
                    System.out.println("Life: " + PlayState.hero.life);
                    //if(PlayState.hero.life <= 0){
                    //    System.exit(0);
                    //}
                }
                hasAttacked--;
        }
    }

    /*! \fn private void AnimUpdate()
        \brief Actualizeaza animatia actuala a eroului.
    */
    @Override
    protected void AnimUpdate(){

        ///Actualizeaza animatia

        switch (type) {
            case "snake":
                switch (dir) {
                    case LEFT:
                        anim_idle = Assets.snakeLeft_idle;
                        anim_running = Assets.snakeLeft;
                        anim_attack = Assets.snakeLeft_attack;
                        break;
                    case RIGHT:
                        anim_idle = Assets.snakeRight_idle;
                        anim_running = Assets.snakeRight;
                        anim_attack = Assets.snakeRight_attack;
                        break;
                }
                break;

            case "scorpio":
                switch (dir) {
                    case LEFT:
                        anim_idle = Assets.scorpioLeft_idle;
                        anim_running = Assets.scorpioLeft;
                        anim_attack = Assets.scorpioLeft_attack;
                        break;
                    case RIGHT:
                        anim_idle = Assets.scorpioRight_idle;
                        anim_running = Assets.scorpioRight;
                        anim_attack = Assets.scorpioRight_attack;
                        break;
                }
                break;

            case "vulture":
                switch (dir) {
                    case LEFT:
                        anim_idle = Assets.vultureLeft_idle;
                        anim_running = Assets.vultureLeft;
                        anim_attack = Assets.vultureLeft_attack;
                        break;
                    case RIGHT:
                        anim_idle = Assets.vultureRight_idle;
                        anim_running = Assets.vultureRight;
                        anim_attack = Assets.vultureRight_attack;
                        break;
                }
                break;

        }

        if(hasAttacked > 0){
            hasAttacked--;
        }
        if(hasAttacked <= 0 &&isIdle > 0 && !isChasingPlayer() ) {
            isIdle--;
        }
        else if(isIdle<=0) {
            anim = anim_idle;
            isIdle--;
            if(isIdle <= -anim.getSize()*2){
                anim.nextFrame();
                isIdle=0;
            }
        }

        if( hasAttacked <=0 && isChasingPlayer()){
            isIdle = 100;
            if(run <= 10 && run >= 0){
                run--;
            }
            else if (run <0){
                anim.nextFrame();
                run = 10;
            }
            anim = anim_running;
        }
        else if (hasAttacked<=0){
            anim = anim_idle;
            run = 10;
        }
    }

    @Override
    public void Update() {
        AnimUpdate();
        Move();
    }

    /*! \fn private boolean isChasingPlayer()
    \brief Metoda ce verifica daca inamicul ar trebui sa urmareasca jucatorul;
    */
    private boolean isChasingPlayer(){
        xMove = 0;
        yMove = 0;
        if(isPlayerNearby() && life >0){
            ///Verificam daca inamicul este intr-o zona suficient de apropiata de jucator incat sa il atace.
            if(abs(PlayState.hero.x - x) <96 && abs(PlayState.hero.y - y) < 96){
                if(PlayState.hero.hasAttacked > 27){
                    life--;
                    System.out.println("MobLife:" + life);
                    if(life <= 0){
                        this.SetY(0);
                        Random rd = new Random();
                        PlayState.hero.gold+=rd.nextInt(5)+1;
                    }
                }
            }
            if(abs(PlayState.hero.x - x) <48 && abs(PlayState.hero.y - y) < 12){
                attack();
                return false;
            }
            if(abs(PlayState.hero.y - this.y) < abs(PlayState.hero.x - this.x)) {
                if (PlayState.hero.x > x) {
                    if(canMoveRight() || type.equals("vulture")){
                        xMove = speed;
                        dir = Direction.RIGHT;
                    }
                } else if (canMoveLeft() || type.equals("vulture")) {
                    xMove = -speed;
                    dir = Direction.LEFT;
                }
            }
            else{
                if(PlayState.hero.y > y ) {
                    if (canMoveDown() || type.equals("vulture")) {
                        yMove = speed;
                    }
                }
                else if (canMoveUp() || type.equals("vulture")) {
                        yMove = -speed;
                    }
            }
            return true;
        }
        return false;
    }

    private boolean isPlayerNearby(){
        return (abs(PlayState.hero.x - this.x) <288 && abs(PlayState.hero.y - this.y) <200);
    }

    @Override
    public void Draw(Graphics g) {
        if(hasAttacked > 0 ) {
            if (hasAttacked % anim.getSize() == 0)
                anim.nextFrame();
        }
            g.drawImage(anim.getFrame(), calculateOffsetX(x,PlayState.hero), calculateOffsetY(y,PlayState.hero), width, height, null);
        }

}
