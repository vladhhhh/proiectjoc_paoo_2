package PaooGame.Items;

import PaooGame.Graphics.Animation;
import PaooGame.RefLinks;
import PaooGame.Tiles.Tile;
import PaooGame.Utils.Direction;

import static java.lang.Math.abs;

/*! \class public abstract class Character extends Item
    \brief Defineste notiunea abstracta de caracter/individ/fiinta din joc.

    Notiunea este definita doar de viata, viteza de deplasare si distanta cu care trebuie sa se
    miste/deplaseze in urma calculelor.
 */
public abstract class Character extends Item
{
    public static final int DEFAULT_LIFE            = 10;   /*!< Valoarea implicita a vietii unui caracter.*/
    public static final float DEFAULT_SPEED         = 3.0f; /*!< Viteza implicita a unu caracter.*/
    public static final int DEFAULT_CREATURE_WIDTH  = 48;   /*!< Latimea implicita a imaginii caracterului.*/
    public static final int DEFAULT_CREATURE_HEIGHT = 48;   /*!< Inaltimea implicita a imaginii caracterului.*/

    protected int life;     /*!< Retine viata caracterului.*/
    protected int attackPoints;     /*!< Retine puterea de atac a caracterului.*/
    protected float speed;  /*!< Retine viteza de deplasare caracterului.*/
    protected float xMove;  /*!< Retine noua pozitie a caracterului pe axa X.*/
    protected float yMove;  /*!< Retine noua pozitie a caracterului pe axa Y.*/
    protected Direction dir; /*!< Retine noua directie a caracterului.*/
    protected int isIdle = 100; /*!< Verifica daca caracterul se misca sau nu.*/
    protected int run;
    protected int hasAttacked = 0; /*!< Verifica daca caracterul a atacat sau nu.*/
    protected Animation anim_idle ; /*!< Animatia caracterului in cazul in care acesta nu se misca.*/
    protected Animation anim_running; /*!< Animatia caracterului in care acesta se deplaseaza.*/
    protected Animation anim_attack; /*!< Animatia caracterului in care acesta se deplaseaza.*/
    protected Animation anim; /*!< Animatia generala a caracterului ce va fi afisata.*/



    /*! \fn public Character(RefLinks refLink, float x, float y, int width, int height)
            \brief Constructor de initializare al clasei Character

            \param refLink Referinta catre obiectul shortcut (care retine alte referinte utile/necesare in joc).
            \param x Pozitia de start pa axa X a caracterului.
            \param y Pozitia de start pa axa Y a caracterului.
            \param width Latimea imaginii caracterului.
            \param height Inaltimea imaginii caracterului.
         */
    public Character(RefLinks refLink, float x, float y, int width, int height)
    {
            ///Apel constructor la clasei de baza
        super(refLink, x,y, width, height);
            //Seteaza pe valorile implicite pentru viata, viteza si distantele de deplasare
        life    = DEFAULT_LIFE;
        speed   = DEFAULT_SPEED;
        xMove   = 0;
        yMove   = 0;
        dir = Direction.LEFT;
        attackPoints = 1;
    }

    /*! \fn public void Move()
        \brief Modifica pozitia caracterului
     */
    public void Move()
    {
            ///Modifica pozitia caracterului pe axa X.
            ///Modifica pozitia caracterului pe axa Y.
        MoveX();
        MoveY();
    }

    /*! \fn public void MoveX()
        \brief Modifica pozitia caracterului pe axa X.
     */
    public void MoveX()
    {
            ///Aduna la pozitia curenta numarul de pixeli cu care trebuie sa se deplaseze pe axa X.
        x += xMove;
    }

    /*! \fn public void MoveY()
        \brief Modifica pozitia caracterului pe axa Y.
     */
    public void MoveY()
    {
            ///Aduna la pozitia curenta numarul de pixeli cu care trebuie sa se deplaseze pe axa Y.
        y += yMove;
    }

    /*! \fn public int GetLife()
        \brief Returneaza viata caracterului.
     */
    public int GetLife()
    {
        return life;
    }

    /*! \fn public int GetSpeed()
        \brief Returneaza viteza caracterului.
     */
    public float GetSpeed()
    {
        return speed;
    }

    /*! \fn public void SetLife(int life)
        \brief Seteaza viata caracterului.
     */
    public void SetLife(int life)
    {
        this.life = life;
    }

    /*! \fn public void SetSpeed(float speed)
        \brief
     */
    public void SetSpeed(float speed) {
        this.speed = speed;
    }

    /*! \fn public float GetXMove()
        \brief Returneaza distanta in pixeli pe axa X cu care este actualizata pozitia caracterului.
     */
    public float GetXMove()
    {
        return xMove;
    }

    /*! \fn public float GetYMove()
        \brief Returneaza distanta in pixeli pe axa Y cu care este actualizata pozitia caracterului.
     */
    public float GetYMove()
    {
        return yMove;
    }

    /*! \fn public void SetXMove(float xMove)
        \brief Seteaza distanta in pixeli pe axa X cu care va fi actualizata pozitia caracterului.
     */
    public void SetXMove(float xMove)
    {
        this.xMove = xMove;
    }

    /*! \fn public void SetYMove(float yMove)
        \brief Seteaza distanta in pixeli pe axa Y cu care va fi actualizata pozitia caracterului.
     */
    public void SetYMove(float yMove)
    {
        this.yMove = yMove;
    }

    /*! \fn public Direction getDir()()
        \brief Obtine ultima directie in care s-a deplasat caracterul.
     */
    public Direction getDir() {
        return dir;
    }

    /*! \fn public void setDir(Direction dir)()
        \brief Seteaza directia in care priveste caracterul.
     */
    public void setDir(Direction dir) {
        this.dir = dir;
    }

    /*! \fn public abstract void attack()
        \brief Metoda abstracta pentru a defini atacul de catre caracter;
    */
    public abstract void attack();

    /*! \fn public abstract void AnimUpdate()
    \brief Metoda abstracta pentru a actualiza animatiile caracterului;
    */
    protected abstract void AnimUpdate();


    /*! \fn protected boolean canMoveUp()
       \brief Verifica daca caracterul se poate misca in sus.
   */
    protected boolean canMoveUp()
    {
        int tileup = (int)(this.GetY()-speed)/48;
        int tileleft = (int)this.GetX()/48;
        if(refLink.GetMap().GetTile(tileleft, tileup).IsSolid() || refLink.GetMap().GetTile((int)(this.x + this.normalBounds.width)/48, tileup).IsSolid())
            return false;

        return true;
    }

    /*! \fn protected boolean canMoveDown()
        \brief Verifica daca caracterul se poate misca in jos.
    */
    protected boolean canMoveDown()
    {
        int tiledown = (int)(this.GetY())/48+1;
        int tileleft = (int)this.GetX()/48;
        if(refLink.GetMap().GetTile(tileleft, tiledown).IsSolid() || refLink.GetMap().GetTile((int)(this.x + this.normalBounds.width)/48, tiledown).IsSolid())
            return false;

        return true;
    }

    /*! \fn protected boolean canMoveLeft()
        \brief Verifica daca caracterul se poate misca in stanga.
    */
    protected boolean canMoveLeft()
    {
        int tileleft = (int)(this.GetX()-speed)/48;
        int tileup = (int)this.GetY()/48;
        if(refLink.GetMap().GetTile(tileleft, tileup).IsSolid() || refLink.GetMap().GetTile(tileleft, (int)(this.y + this.normalBounds.width)/48).IsSolid())
            return false;

        return true;
    }

    /*! \fn protected boolean canMoveUp()
        \brief Verifica daca caracterul se poate misca in dreapta.
    */
    protected boolean canMoveRight()
    {
        int tiledown = (int)(this.GetY())/48;
        int tileleft = (int)this.GetX()/48+1;
        if(refLink.GetMap().GetTile(tileleft, tiledown).IsSolid() || refLink.GetMap().GetTile(tileleft, (int)(this.y + this.normalBounds.width)/48).IsSolid())
            return false;

        return true;
    }

    /*! \fn protected int calculateOffsetX(int x, Item ent)
    \brief Calculeaza marginile imaginii caracterului ce trebuie desenate in functie de pozitia jucatorului, pe axa OX.

    \param x Un numar, reprezentand locatia jucatorului in matricea de coduri.
    \param Item Entitatea fata de care se va face calcularea marginilor.
    */
    protected int calculateOffsetX(float x, Item ent) {
        return (int) (x +((refLink.GetWidth() / 2) - ent.GetX()-(Tile.TILE_WIDTH / 2)))+24;
    }

    /*! \fn protected int calculateOffsetY(int Y, Item ent)
    \brief Calculeaza marginile imaginii caracterului ce trebuie desenate in functie de pozitia jucatorului, pe axa OY.

    \param Y Un numar, reprezentand locatia jucatorului in matricea de coduri.
    \param Item Entitatea fata de care se va face calcularea marginilor.
    */
    protected int calculateOffsetY(float y, Item ent) {
        return (int) (y +((refLink.GetHeight() / 2) - ent.GetY() -(Tile.TILE_HEIGHT / 2)))+24;
    }


}

