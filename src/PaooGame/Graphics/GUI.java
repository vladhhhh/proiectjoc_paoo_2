package PaooGame.Graphics;

import PaooGame.States.PlayState;
import PaooGame.Tiles.Tile;


import java.awt.*;

import static PaooGame.States.PlayState.hero;


    /*! \class public class GUI
    \brief Clasa incarca fiecare element grafic necesar interfetei jocului.

    Clasa include toate elementele necesare desenarii fiecarei stari ale jocului, in timpul jocului, in meniu, in setari, etc.
    */
public class GUI {
        private static int counter=1000;

    /*! \fn public static void drawPlayState(Graphics g)
    \brief Functia deseneaza toate elementele interfetei grafice din timpul jocului.

    Aceasta functie deseneaza diferite informatii despre jucator, cum ar fi viata acestuia, arma pe care o foloseste
    si ce atac are aceasta, etc.
     */

    public static void drawPlayState(Graphics g) {
            //g.setColor(Color.BLACK);
            //g.fillRect(24, 8, 16 * 10+8, 32);
        /// Desenare contur pe care se afla inimile
        g.drawImage(Assets.GUI_blackBg,24,8,null);


        /// Desenare animatia banutului rotitor
        if(counter!=0) {
            if (counter % 25 == 0 && counter<=500) {
                Assets.coins.nextFrame();
            }
        }
        else
        {
            counter=1000;
        }
        g.drawImage(Assets.coins.getFrame(), 24, 50, 32, 32, null);
        counter--;

        /// Desenare numar de bani
        g.drawString(String.valueOf(PlayState.hero.getGold()),36,68);

        /// Desenare numarului de inimi
        if (hero.GetLife() > 0) {
            if (hero.GetLife() % 2 == 0) {
                for (int i = 0; i < hero.GetLife(); i += 2) {
                    g.drawImage(Assets.GUI_fullHeart, 48 + (i - 1) * Tile.TILE_WIDTH / 3, 16, Tile.TILE_WIDTH / 3, Tile.TILE_HEIGHT / 3, null);
                }
            } else {
                for (int i = 0; i < hero.GetLife() - 1; i += 2) {
                    g.drawImage(Assets.GUI_fullHeart, 48 + (i - 1) * Tile.TILE_WIDTH / 3, 16, Tile.TILE_WIDTH / 3, Tile.TILE_HEIGHT / 3, null);
                }
                g.drawImage(Assets.GUI_halfHeart, 16 + hero.GetLife() * Tile.TILE_WIDTH / 3, 16, Tile.TILE_WIDTH / 3, Tile.TILE_HEIGHT / 3, null);
            }
        }
    }
}
