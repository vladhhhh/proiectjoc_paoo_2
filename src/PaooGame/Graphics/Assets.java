package PaooGame.Graphics;

import PaooGame.Utils.Functions;

import java.awt.image.BufferedImage;

/*! \class public class Assets
    \brief Clasa incarca fiecare element grafic necesar jocului.

    Game assets include tot ce este folosit intr-un joc: imagini, sunete, harti etc.
 */
public class Assets
{
        /// Referinte catre elementele grafice (dale) utilizate in joc.
    public static Animation heroLeft ;
    public static Animation heroLeft_idle;
    public static Animation heroRight;
    public static Animation heroRight_idle;
    public static Animation sword_anim_right;
    public static Animation sword_anim_left;
    public static Animation sword_anim_up;
    public static Animation sword_anim_down;
    public static Animation snakeLeft_idle;
    public static Animation snakeRight_idle;
    public static Animation snakeRight;
    public static Animation snakeRight_attack;
    public static Animation snakeLeft;
    public static Animation snakeLeft_attack;

    public static Animation scorpioLeft_idle;
    public static Animation scorpioRight_idle;
    public static Animation scorpioRight;
    public static Animation scorpioRight_attack;
    public static Animation scorpioLeft;
    public static Animation scorpioLeft_attack;

    public static Animation vultureLeft_idle;
    public static Animation vultureRight_idle;
    public static Animation vultureRight;
    public static Animation vultureRight_attack;
    public static Animation vultureLeft;
    public static Animation vultureLeft_attack;


    public static BufferedImage soil;
    public static BufferedImage grass;
    public static BufferedImage mountain;
    public static BufferedImage townGrass;
    public static BufferedImage townGrassDestroyed;
    public static BufferedImage townSoil;
    public static BufferedImage water;
    public static BufferedImage rockUp;
    public static BufferedImage rockDown;
    public static BufferedImage rockLeft;
    public static BufferedImage rockRight;
    public static BufferedImage tree;
    public static BufferedImage tent;
    public static BufferedImage GUI_halfHeart;
    public static BufferedImage GUI_fullHeart;
    public static BufferedImage GUI_blackBg;
    public static Animation coins;

    /*! \fn public static void Init()
        \brief Functia initializaza referintele catre elementele grafice utilizate.

        Aceasta functie poate fi rescrisa astfel incat elementele grafice incarcate/utilizate
        sa fie parametrizate. Din acest motiv referintele nu sunt finale.
     */
    public static void Init()
    {
            /// Se creaza temporar un obiect SpriteSheet initializat prin intermediul clasei ImageLoader
        SpriteSheet sheet = new SpriteSheet(ImageLoader.LoadImage("/textures/PaooGameSpriteSheet.png"));
        BufferedImage img = ImageLoader.LoadImage("/textures/Hill0.png");
        BufferedImage desert = ImageLoader.LoadImage("/textures/desert.png");


            /// Imaginile corespunzatoare pentru GUI
        GUI_halfHeart = ImageLoader.LoadImage("/textures/GUI/halfHeart2.png");
        GUI_fullHeart = ImageLoader.LoadImage("/textures/GUI/fullHeart2.png");
        GUI_blackBg = ImageLoader.LoadImage("/textures/GUI/blackBg.png");
        coins = new Animation(ImageLoader.LoadImage("/textures/GUI/coins.png"),16,4);

            /// Se creeaza animatiile corespunzatoare eroului.
        heroRight = new Animation(ImageLoader.LoadImage("/textures/knight_run_spritesheet.png"),16,6);
        sword_anim_right = new Animation(ImageLoader.LoadImage("/textures/test_atac.png"),16,4);
        sword_anim_left = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/test_atac.png"),16,4));
        sword_anim_up = new Animation(ImageLoader.LoadImage("/textures/test_atac_up_96x32.png"),32,3);
        sword_anim_down = new Animation(ImageLoader.LoadImage("/textures/test_atac_up_96x32_down.png"),32,3);
        heroLeft = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/knight_run_spritesheet.png"),16,6));
        heroRight_idle = new Animation(ImageLoader.LoadImage("/textures/knight_idle_spritesheet.png"),16,6);
        heroLeft_idle = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/knight_idle_spritesheet.png"),16,6));
        snakeLeft_idle = new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_idle - Copy.png"),48,4);
        snakeRight_idle = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_idle - Copy.png"),48,4));
        snakeRight = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_walk.png"),48,4));
        snakeLeft = new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_walk.png"),48,4);
        snakeRight_attack = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_attack.png"),48,6));
        snakeLeft_attack = new Animation(ImageLoader.LoadImage("/textures/enemies/Snake/Snake_attack.png"),48,6);

        scorpioLeft_idle = new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_idle.png"),48,4);
        scorpioRight_idle = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_idle.png"),48,4));
        scorpioRight = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_walk.png"),48,4));
        scorpioLeft = new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_walk.png"),48,4);
        scorpioRight_attack = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_attack.png"),48,4));
        scorpioLeft_attack = new Animation(ImageLoader.LoadImage("/textures/enemies/Scorpio/Scorpio_attack.png"),48,4);

        vultureLeft_idle = new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_idle.png"),48,4);
        vultureRight_idle = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_idle.png"),48,4));
        vultureRight = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_walk.png"),48,4));
        vultureLeft = new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_walk.png"),48,4);
        vultureRight_attack = Functions.reverse_spritesheet_img(new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_attack.png"),48,4));
        vultureLeft_attack = new Animation(ImageLoader.LoadImage("/textures/enemies/Vulture/Vulture_attack.png"),48,4);



            /// Se obtin subimaginile corespunzatoare elementelor necesare.
        grass = desert.getSubimage(80,0,64,48);
        tent = desert.getSubimage(144,100,47,44);
        soil = sheet.crop(1, 0);
        water = sheet.crop(2, 0);
        mountain = desert.getSubimage(142,0,48,48);
        townGrass = sheet.crop(0, 1);
        townGrassDestroyed = sheet.crop(1, 1);
        townSoil = sheet.crop(2, 1);
        tree = desert.getSubimage(80,150,21,41);
        rockUp = sheet.crop(2, 2);
        rockDown = sheet.crop(3, 2);
        rockLeft = sheet.crop(0, 3);
        rockRight = sheet.crop(1, 3);
    }
}
