package PaooGame.Graphics;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/*! \class public class Animation
    \brief Clasa genereaza si contine numarul de imagini dintr-o animatie.

    Aceasta clasa este folosita pentru animatiile caracterelor si itemelor din joc, folositoare astfel pentru
    orice obiect non-static (din punct de vedere al miscarii).
 */
public class Animation extends SpriteSheet {
    private ArrayList<BufferedImage> ani;  /*!< Referinta catre lista de imagini ale animatiei.*/
    private int frame = 0;  /*!< Referinta catre imaginea curenta din animatie.*/

    /*! \fn public void setFrame(int frame,BufferedImage buffImg)
    \brief Functie pentru a schimba o anumite imagine dintr-o animatie cu alta imagine.

    /param frame Indexul imaginii din animatie.
    /param buffImg Imaginea ce va inlocui imaginea din animatia respectiva.
    */
    public void setFrame(int frame,BufferedImage buffImg) {
        this.ani.set(frame,buffImg);
    }

    /*! \fn public void nextFrame()
    \brief Functie pentru a face tranzitia imaginilor dintr-o animatie.

    */
    public void nextFrame(){
        this.frame++;
        if(this.frame == this.ani.size()){
            this.frame = 0;
        }
    }

    /*! \fn public void resetAni()
    \brief Functie folosita pentru a reseta o anumita animatie, pentru a o incepe din nou.

    */
    public void resetAni(){
        this.frame = 0;
    }

    /*! \fn public Animation(BufferedImage buffImg, int tile_size, int tiles_nr)
    \brief Constructorul folosit pentru animatii.

    /param buffImg Imaginea/Spritesheetul ce contine toate imaginile dintr-o animatie.
    /param tile_size Dimensiunea fiecarei imagini ce reprezinta un cadru din animatie.
    /param tiles_nr Numarul de cadre din animatie.
    */
    public Animation(BufferedImage buffImg, int tile_size, int tiles_nr){
        super(buffImg);
        this.ani = new ArrayList<>(tiles_nr);
        for (int i = 0; i < tiles_nr; i++) {
            this.ani.add(buffImg.getSubimage(i*tile_size,0,tile_size,tile_size));
        }
        this.frame = 0;
    }

    /*! \fn public BufferedImage getFrame()
    \brief Functie folosita pentru a obtine o anumite imagine dintr-o animatie.

    */
    public BufferedImage getFrame(){
        return this.ani.get(frame);
    }

    /*! \fn public int getSize()
    \brief Functie folosita pentru a obtine numarul de cadre dintr-o animatie.

    */
    public int getSize(){
        return ani.size();
    }
}
