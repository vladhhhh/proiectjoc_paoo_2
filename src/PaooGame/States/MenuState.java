package PaooGame.States;

import PaooGame.RefLinks;

import java.awt.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;

/*! \class public class MenuState extends State
    \brief Implementeaza notiunea de menu pentru joc.
 */
public class MenuState extends State
{
    private int[] options;
    private String[] options_text;
    private int selected;
    private int wait;

    /*! \fn public MenuState(RefLinks refLink)
        \brief Constructorul de initializare al clasei.

        \param refLink O referinta catre un obiect "shortcut", obiect ce contine o serie de referinte utile in program.
     */
    public MenuState(RefLinks refLink)
    {
            ///Apel al constructorului clasei de baza.
        super(refLink);
        options = new int[5];
        options_text = new String[5];
        Arrays.fill(options, 0);
        options[0] = 1;
        selected = 0;
        wait = 0;
        Arrays.fill(options_text,null);
        options_text[0] = "New Game";
        options_text[1] = "Load Game";
        options_text[2] = "Settings";
        options_text[3] = "About";
        options_text[4] = "Exit";
    }
    /*! \fn public void Update()
        \brief Actualizeaza starea curenta a meniului.
     */
    @Override
    public void Update()
    {
        getInput();
    }

    /*! \fn public void Draw(Graphics g)
        \brief Deseneaza (randeaza) pe ecran starea curenta a meniului.

        \param g Contextul grafic in care trebuie sa deseneze starea jocului pe ecran.
     */
    @Override
    public void Draw(Graphics g)
    {
        int i = 0;
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0,0, refLink.GetWidth(), refLink.GetHeight());
        g.setColor(Color.WHITE);
        while(i< options_text.length){
            if(i == selected){
                g.setColor(Color.GREEN);
                g.drawString(options_text[i],100,100+25*i );
                g.setColor(Color.WHITE);
            }
            else {
                g.drawString(options_text[i],100,100+25*i );
            }
            i++;
        }
    }

    /*! \fn public void getInput()
    \brief Actualizeaza optiunea curenta a meniului.

    Verifica daca a fost apasata o tasta, fie pentru schimbarea meniului, fie pentru alegerea
    unei optiuni, si astepta un anumit timp pentru a putea face alta alegere, deoarece
    altfel nu s-ar putea alege cu precizie o anumita optiune.
    */
    private void getInput() {
        if (wait == 0) {
            if (refLink.GetKeyManager().down) {
                if (selected < 4) {
                    options[selected] = 0;
                    selected++;
                    options[selected] = 1;
                }
            }

            if (refLink.GetKeyManager().up) {
                if (selected > 0) {
                    options[selected] = 0;
                    selected--;
                    options[selected] = 1;
                }
            }

            if(refLink.GetKeyManager().enter){
                switch (selected){
                    case 0: State.SetState(refLink.GetGame().playState); break;
                    case 1: LoadGame(); break;
                    case 2: State.SetState(refLink.GetGame().settingsState); break;
                    case 3: State.SetState(refLink.GetGame().aboutState); break;
                    case 4: System.exit(0); break;
                }
            }
            wait = 4;
        }
        else{
            wait--;
        }

    }

    /*! \fn private void LoadGame()
    \brief Incarca ultima salvare a jocului, atat din DB cat si harta din fisier.
    */
    private void LoadGame(){
        Statement st = null;
        Connection c = refLink.getConnection();
        try {
            st = c.createStatement();
            String sql = "SELECT * FROM PLAYER_INFO WHERE ID = 1;";
            /// Se incarca din DB datele despre erou
            ResultSet rs = st.executeQuery(sql);
            PlayState.hero.SetX(rs.getFloat("X"));
            PlayState.hero.SetY(rs.getFloat("Y"));
            PlayState.hero.SetLife(rs.getInt("HP"));
            PlayState.hero.setGold(rs.getInt("GOLD"));
            /// Se incarca din DB datele despre inamici
            sql = "SELECT * FROM ENEMIES;";
            rs = st.executeQuery(sql);
            for (int i = 0; i < PlayState.enemies.length; i++) {
                PlayState.enemies[rs.getInt("ID")].SetX(rs.getFloat("X"));
                PlayState.enemies[rs.getInt("ID")].SetY(rs.getFloat("y"));
                PlayState.enemies[rs.getInt("ID")].SetLife(rs.getInt("HP"));
                PlayState.enemies[rs.getInt("ID")].type = rs.getString("TYPE");
                rs.next();
            }
            st.close();
        }
        catch (Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
        /// Se incarca harta din fisier
        refLink.GetMap().LoadWorld();
        /// Se trece in starea de joc.
        State.SetState(refLink.GetGame().playState);
    }
}
