package PaooGame.States;

import PaooGame.Graphics.GUI;
import PaooGame.Items.Enemy;
import PaooGame.Items.Hero;
import PaooGame.RefLinks;
import PaooGame.Maps.Map;

import java.awt.*;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Random;

/*! \class public class PlayState extends State
    \brief Implementeaza/controleaza jocul.
 */
public class PlayState extends State
{
    public static Hero hero;  /*!< Referinta catre obiectul animat erou (controlat de utilizator).*/
    private Map map;    /*!< Referinta catre harta curenta.*/
    public static Enemy[] enemies;
    private static String[] enemies_type;

    /*! \fn public PlayState(RefLinks refLink)
        \brief Constructorul de initializare al clasei

        \param refLink O referinta catre un obiect "shortcut", obiect ce contine o serie de referinte utile in program.
     */
    public PlayState(RefLinks refLink)
    {
            ///Apel al constructorului clasei de baza
        super(refLink);
            ///Construieste harta jocului
        map = new Map(refLink);
            ///Referinta catre harta construita este setata si in obiectul shortcut pentru a fi accesibila si in alte clase ale programului.
        refLink.SetMap(map);
            ///Construieste eroul
        createHero();
            /// Creeaza inamicii
        createEnemies();

    }

    /*! \fn public void Update()
        \brief Actualizeaza starea curenta a jocului.
     */
    @Override
    public void Update()
    {
        map.Update();
        int i=0;
        for(;i< enemies.length;++i){
            enemies[i].Update();
        }
        hero.Update();
    }

    /*! \fn public void Draw(Graphics g)
        \brief Deseneaza (randeaza) pe ecran starea curenta a jocului.

        \param g Contextul grafic in care trebuie sa deseneze starea jocului pe ecran.
     */
    @Override
    public void Draw(Graphics g)
    {
        map.Draw(g,hero);
        int i=0;
        for(;i< enemies.length;++i){
            enemies[i].Draw(g);
        }
        hero.Draw(g);
        GUI.drawPlayState(g);
    }

    public static void SaveGame_Enemies()
    {
        Statement st = null;
        Connection c = refLink.getConnection();
        for (int i = 0; i < enemies.length; i++) {
            try {
                st = c.createStatement();
                String sql = "UPDATE ENEMIES set X = " + enemies[i].GetX() + " WHERE ID = " + i + ";";
                System.out.println("UPDATE X " + st.executeUpdate(sql));
                sql = "UPDATE ENEMIES set Y = " + enemies[i].GetY() + " WHERE ID = " + i + ";";
                System.out.println("UPDATE Y " + st.executeUpdate(sql));
                sql = "UPDATE ENEMIES set HP = " + enemies[i].GetLife() + " WHERE ID = " + i + ";";
                System.out.println("UPDATE HP " + st.executeUpdate(sql));
                st.close();
                c.commit();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
        }
    }


    public void createHero(){
        int zx = 0,zy = 0;
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                if(map.getTiles()[i][j] == 1){
                    zx = j;
                    zy = i;
                    i=100;
                    j=100;
                }

            }
        }
        hero = new Hero(refLink,zx*48, zy*48);
    }

    public void createEnemies() {
        Random rd = new Random();
        enemies_type = new String[3];
        enemies_type[0] = "snake";
        enemies_type[1] = "scorpio";
        enemies_type[2] = "vulture";
        enemies = new Enemy[10];
        for (int i = 0; i < enemies.length; i++) {
            int sx, sy;
            sx = rd.nextInt(77) + 11;
            sy = rd.nextInt(77) + 11;
            while (map.getTiles()[sx][sy] != 1) {
                sx = rd.nextInt(77) + 11;
                sy = rd.nextInt(77) + 11;
            }
            enemies[i] = new Enemy(refLink, 48 * sy, 48 * sx, enemies_type[rd.nextInt(3)]);
            Statement st = null;
            Connection c = refLink.getConnection();
            try {
                st = c.createStatement();
                String sql = "INSERT INTO ENEMIES (ID,X,Y,HP,TYPE) VALUES ("+ i + "," + enemies[i].GetX() + "," + enemies[i].GetY() + "," + enemies[i].GetLife() + "," + "'" + enemies[i].type + "'" + ");";
                System.out.println("enemy insert " + st.executeUpdate(sql));
                st.close();
                c.commit();
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }
        }
    }
}
