package PaooGame.Maps;

import PaooGame.Items.Hero;
import PaooGame.Items.*;
import PaooGame.RefLinks;
import PaooGame.Tiles.*;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

/*! \class public class Map
    \brief Implementeaza notiunea de harta a jocului.
 */
public class Map {
    private RefLinks refLink;   /*!< O referinte catre un obiect "shortcut", obiect ce contine o serie de referinte utile in program.*/
    private int width=100;          /*!< Latimea hartii in numar de dale.*/
    private int height=100;         /*!< Inaltimea hartii in numar de dale.*/
    private int[][] tiles;     /*!< Referinta catre o matrice cu codurile dalelor ce vor construi harta.*/

    /*! \fn public Map(RefLinks refLink)
        \brief Constructorul de initializare al clasei.

        \param refLink O referinte catre un obiect "shortcut", obiect ce contine o serie de referinte utile in program.
     */
    public Map(RefLinks refLink) {
        /// Retine referinta "shortcut".
        this.refLink = refLink;
        ///incarca harta de start. Functia poate primi ca argument id-ul hartii ce poate fi incarcat.
        CreateWorld();
    }

    /*! \fn public  void Update()
        \brief Actualizarea hartii in functie de evenimente (un copac a fost taiat)
     */
    public void Update() {

    }

    public int[][] getTiles() {
        return tiles;
    }

    /*! \fn public void Draw(Graphics g)
            \brief Functia de desenare a hartii.

            \param g Contextl grafi in care se realizeaza desenarea.
         */
    public void Draw(Graphics g, Hero h) {
        int ssx = 22;
        int ssy = 12;
        int cx;
        int cy;
        int mx = height;
        int my = width;
        if ((int)(h.GetX()/Tile.TILE_WIDTH) < ssx/2) {
            cx = 0;
        } else if ((int)h.GetX()/Tile.TILE_WIDTH > mx - ssx / 2) {
            cx = (mx - ssx);
        } else {
            cx = (int) (h.GetX()/Tile.TILE_WIDTH - ssx / 2);
        }

        if ((int)h.GetY()/Tile.TILE_HEIGHT < ssy / 2) {
            cy = 0;
        } else if ((int)h.GetY()/Tile.TILE_HEIGHT > my - ssy / 2) {
            cy = (my - ssy);
        } else {
            cy = (int) (h.GetY()/Tile.TILE_HEIGHT - ssy / 2);
        }

        ///Se parcurge matricea de dale (codurile aferente) si se deseneaza harta respectiva
        for (int y = cy; y < cy+ ssy; y++) {
            for (int x = cx; x < cx+ ssx; x++) {
                GetTile(x, y).Draw(g, calculateOffsetX(x, h), calculateOffsetY(y, h));
            }
        }
    }


    /*! \fn private int calculateOffsetX(int x, Item ent)
        \brief Calculeaza marginile hartii ce trebuie desenate in functie de pozitia jucatorului, pe axa OX.

        \param x Un numar, reprezentand locatia jucatorului in matricea de coduri.
        \param Item Entitatea fata de care se va face calcularea marginilor.
     */
    private int calculateOffsetX(int x, Item ent) {
        return (int) (x*Tile.TILE_WIDTH +((refLink.GetWidth() / 2) - ent.GetX()));
    }

    /*! \fn private int calculateOffsetY(int Y, Item ent)
    \brief Calculeaza marginile hartii ce trebuie desenate in functie de pozitia jucatorului, pe axa OY.

    \param Y Un numar, reprezentand locatia jucatorului in matricea de coduri.
    \param Item Entitatea fata de care se va face calcularea marginilor.
 */
    private int calculateOffsetY(int y, Item ent) {
        return (int) (y*Tile.TILE_HEIGHT+((refLink.GetHeight() / 2) - ent.GetY()));
    }

    /*! \fn public Tile GetTile(int x, int y)
        \brief Intoarce o referinta catre dala aferenta codului din matrice de dale.

        In situatia in care dala nu este gasita datorita unei erori ce tine de cod dala, coordonate gresite etc se
        intoarce o dala predefinita (ex. grassTile, mountainTile)
     */
    public Tile GetTile(int x, int y) {
        if (x < 0 || y < 0 || x >= width || y >= height) {
            return Tile.grassTile;
        }
        Tile t = Tile.tiles[tiles[y][x]];
        if (t == null) {
            return Tile.mountainTile;
        }
        return t;
    }

    /*! \fn private void CreateWorld()
        \brief Functie de generare a hartii jocului.
        Aceasta va fi generata intamplator la fiecare deschidere a jocului
     */
    private void CreateWorld() {
        ///Se stabileste latimea hartii in numar de dale.
        width = 100;
        ///Se stabileste inaltimea hartii in numar de dale
        height = 100;
        ///Se construieste matricea de coduri de dale
        tiles = new int[width][height];
        ///Se genereaza matricea folosind algoritmul "Random Walk" modificat
        tiles = genereaza_random_walk(height, width, 12000);
    }

    /*! \fn private int[][] genereaza_random_walk(int lungime, int latime, int tunele_max)
        \brief Generarea unei harti aleatoare pentru fiecare nivel.

        \param lungime Lungimea hartii generate in numar de dale.
        \param latime Latimea hartii generate in numar de dale.
        \param tunele_max Numarul total de tunele generate in interiorul intregii harti.

        La fiecare deschidere a jocului, la fiecare pornire, daca se incepe un joc nou, se va genera automat,
        aleator, o harta pentru jucator, prin intermediul algoritmului "Random Walk" modificat.
     */

    private int[][] genereaza_random_walk(int lungime, int latime, int tunele_max) {
        System.out.println("HARTA - S-a generat harta");
        int[][] map = new int[latime][lungime];
        for (int[] ints : map) {
            Arrays.fill(ints, 0);
        }
        Random rd = new Random();
        int current_row = (rd.nextInt((latime - 12))) + 6;
        int current_collumn = (rd.nextInt((lungime - 22))) + 11;
        map[current_row][current_collumn] = 1;
        int dir;
        /*
        dir = 0 -> y+=1, x=0
        dir = 1 -> y+=0, x+=1
        dir = 2 -> y-=1, x+=0
        dir = 3 -> y+=0, x-=1
         */
        while (tunele_max >= 0) {
            dir = rd.nextInt(4);
            switch (dir) {
                case 0: {
                    if (current_row + 1 <= latime - 6) {
                        map[current_row + 1][current_collumn] = 1;
                        current_row++;
                        tunele_max--;
                    }
                    break;
                }
                case 1: {
                    if (current_collumn + 1 <= lungime - 12) {
                        map[current_row][current_collumn + 1] = 1;
                        current_collumn++;
                        tunele_max--;
                    }
                    break;
                }
                case 2: {
                    if (current_row - 1 >= 6) {
                        map[current_row - 1][current_collumn] = 1;
                        current_row--;
                        tunele_max--;
                    }
                    break;
                }
                case 3: {
                    if (current_collumn - 1 >= 11) {
                        map[current_row][current_collumn - 1] = 1;
                        current_collumn--;
                        tunele_max--;
                    }
                    break;
                }
            }
        }
        for (int i = 0; i < latime; i++) {
            for (int j = 0; j < lungime; j++) {
                boolean onBorder = i == 0 || j == 0 || i == (latime - 1) || j == (lungime - 1);
                if (onBorder) {
                    map[i][j] = 0;
                }
                if(!onBorder && map[i][j] ==0 && map[i-1][j]==1 && map[i+1][j]==1 && map[i][j-1]==1 && map[i][j+1]==1){
                    if(rd.nextInt(100)<=50){
                        map[i][j] = 3;
                    }
                    else
                        map[i][j] =4;
                }
            }
        }
        for (int i = latime - 1; i > 0; i--) {
            for (int j = lungime - 1; j > 0; j--) {
                if (map[i][j] == 1) {
                    map[i][j] = 2;
                    i = 0;
                    break;
                }
            }
        }
        //afiseaza_consola(map);
        return map;

    }

    /*! \fn private void afiseaza_consola(int[][] map)
    \brief Afiseaza harta sub forma de coduri(0, 1, 2..) in consola

    \param map[] Harta ce trebuie afisata in consola.
    */
    private void afiseaza_consola(int[][] map) {
        for (int[] ints : map) {
            for (int anInt : ints) {
                System.out.print(anInt);
            }
            System.out.println();
        }
    }

    /*! \fn public void SaveGame_Map()
    \brief Salveaza harta in fisier "harta.txt"
    */
    public void SaveGame_Map() {
        FileWriter f = null;
        /// Se utilizeaza pentru a prinde exceptiile de eroare la deschiderea fisierului
        try
        {
            /// Fisier deschis pt a scrie
            f = new FileWriter("harta.txt");
            for (int[] ints : tiles) {
                for (int anInt : ints) {
                    f.write(String.valueOf(anInt));
                }
            }
            f.close();
        }
        catch (IOException e)
        {
            System.out.println("Eroare la scriere");
        }
    }

    /*! \fn public void LoadWorld()
    \brief Incarca harta din fisierul "harta.txt"
    */
    public void LoadWorld() {
        int c,k=0;
        FileReader f = null;
        /// Se utilizeaza pentru a prinde exceptiile de eroare la deschiderea fisierului
        try {
            f = new FileReader("harta.txt");
            while ((c = f.read()) != -1)
            {
                tiles[k/100][k%100]=Integer.parseInt(String.valueOf(c))-48;
                ++k;
            }
            f.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Fisierul nu a fost gasit");
        }
        catch (IOException e)
        {
            System.out.println("Eroare la citire");
        }
    }


}